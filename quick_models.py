def profitloss_report():
  profitLossModel = {
        "account": "VARCHAR(255) NOT NULL",
        "date": "varchar(50) not null",
        "money": "float8",
      }
    
  profitLoss = "create table if not exists profitloss_report("

  for k in profitLossModel.keys() :
    profitLoss += '"{}" {}, '.format(k,profitLossModel[k])
    
  profitLoss = profitLoss[:-2]+");"
  return profitLoss