from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
import psycopg2
import json
import shiphero_models
import time

conn = psycopg2.connect(host="localhost",database="bi",user="postgres",password="Jyotima@123",port=5432)

cursor = conn.cursor()

def callApi(table) :
    
    try : 
      _transport = RequestsHTTPTransport( url='https://public-api.shiphero.com/graphql', use_json=True, )
      _transport.headers = { "User-Agent": "Mozilla/5.0 (X11; buntu; " + "Linux x86_64; rv:58.0) Gecko/0100101 Firefox/58.0", "Authorization": "Bearer {}".format('eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUQXlOVU13T0Rrd09ETXhSVVZDUXpBNU5rSkVOVVUxUmtNeU1URTRNMEkzTWpnd05ERkdNdyJ9.eyJodHRwOi8vc2hpcGhlcm8tcHVibGljLWFwaS91c2VyaW5mbyI6eyJuYW1lIjoiaG11c2luZ3V6aUB3a3N5c3RlbXMubmV0Iiwibmlja25hbWUiOiJobXVzaW5ndXppIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzlhODZkZjY2NTk0MzgyOWJhNzQxYzA3NWIxYmYwMmM0P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGaG0ucG5nIiwiYWNjb3VudF9pZCI6IjY0Mjc0IiwiaXNfYWNjb3VudF9hZG1pbiI6ZmFsc2V9LCJpc3MiOiJodHRwczovL2xvZ2luLnNoaXBoZXJvLmNvbS8iLCJzdWIiOiJhdXRoMHwyMjA2MzAiLCJhdWQiOlsic2hpcGhlcm8tcHVibGljLWFwaSJdLCJpYXQiOjE2NTgyMzU1MzQsImV4cCI6MTY2MDY1NDczNCwiYXpwIjoibXRjYndxSTJyNjEzRGNPTjNEYlVhSExxUXpRNGRraG4iLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIHZpZXc6cHJvZHVjdHMgY2hhbmdlOnByb2R1Y3RzIHZpZXc6b3JkZXJzIGNoYW5nZTpvcmRlcnMgdmlldzpwdXJjaGFzZV9vcmRlcnMgY2hhbmdlOnB1cmNoYXNlX29yZGVycyB2aWV3OnNoaXBtZW50cyBjaGFuZ2U6c2hpcG1lbnRzIHZpZXc6cmV0dXJucyBjaGFuZ2U6cmV0dXJucyB2aWV3OndhcmVob3VzZV9wcm9kdWN0cyBjaGFuZ2U6d2FyZWhvdXNlX3Byb2R1Y3RzIHZpZXc6cGlja2luZ19zdGF0cyB2aWV3OnBhY2tpbmdfc3RhdHMgb2ZmbGluZV9hY2Nlc3MiLCJndHkiOlsicmVmcmVzaF90b2tlbiIsInBhc3N3b3JkIl19.bEA4_dJghxIhJrqYs2oOEXkA9d1pnKolj2gZbmvvkXI5JUXKsHUJhnPWe8FKEWftnmTvy9Oaw8QMIGdFhmMkJl8fEhyUH5Ix2OOAy5bY2V2Nl-7s5e3mbONs9noe1q5xixKQkEeNTjbpk7Zq487e5ZYc4b6aVA5M8RGmx6txocwi_yZgAcwvIwXHjFTxkB3VCPQ0DicL4cHF69o6dNyJjirmEV4gK0QSEFG1Q-0JA7AmcW8wIpBnZHbBteOH1BPP57s-xFec2xEePSTiVH496rxYSkvxUrpyfIPWsnPnSd2VQ-VnbIdH93T4fV9mJTUWy08hzj6uX4bCdN2F4rV2bw'), "content-type": "application/json", }
      client = Client( transport=_transport, fetch_schema_from_transport=True, )
      query = gql(""" {
  orders {
    request_id
    complexity
    data(first: 5) {
      edges {
        node {
          id
          legacy_id
          order_number
          partner_order_id
          shop_name
          fulfillment_status
          order_date
          total_tax
          subtotal
          total_discounts
          total_price
          auto_print_return_label
          custom_invoice_url
          account_id
          email
          profile
          packing_note
          required_ship_date
          flagged
          saturday_delivery
          ignore_address_validation_errors
          priority_flag
          source
          allow_partial
          require_signature
          adult_signature_required
          alcohol
          expected_weight_in_oz
          insurance
          insurance_amount
          currency
          has_dry_ice
          allocation_priority
          allow_split
          line_items(first: 10) {
            edges {
              node {
                sku
                quantity
              }
            }
          }
        }
      }
    }
  }
} """)

      orderData = client.execute(query)
      resultData = orderData['orders']['data']['edges']
      salesData(table, resultData)

    except Exception as e :
        print(str(e))
        exit()


def salesData(table, allData) :
       
        #Data processing starts
        createTable = getattr(shiphero_models, '%s' % table)()
        cursor.execute(createTable)
        conn.commit()
        #for sales in allData['SaleList'] :
        # data = json.dumps(allData)
        length = len(allData)
        orders = {'records': []}
        listItems = []
        for i in range(length):
          orders['records'].append(allData[i]['node'])
          listLength = len(allData[i]['node']['line_items']['edges'])
          for j in range(listLength):
            itemdata = {
              "id" : allData[i]['node']['id'],
              "sku": allData[i]['node']['line_items']['edges'][j]['node']['sku'],
              "quantity" : allData[i]['node']['line_items']['edges'][j]['node']['quantity']
            }
            listItems.append(itemdata)  

        
        query_sql = """ insert into """+table+""" select * from json_populate_recordset(NULL::"""+table+""", %s) """
        print(query_sql)
        cursor.execute(query_sql, (json.dumps(orders['records']),))
        conn.commit()
        time.sleep(2)
        # insert data into listItems
        query_sql = """ insert into shipOrdersLineitems select * from json_populate_recordset(NULL::shipOrdersLineitems, %s) """
        print(query_sql)
        cursor.execute(query_sql, (json.dumps(listItems),))
        conn.commit()
        time.sleep(2)
        exit()

callApi('ship_orders')        
# length = len(resultData)
# for i in range(length):
#   postgres_insert_query = """ INSERT INTO shiphero_data (category,data) VALUES (%s,%s)"""
#   record_to_insert = ('orders', json.dumps(resultData[i]))
#   cursor.execute(postgres_insert_query, record_to_insert)
#   conn.commit()
#   count = cursor.rowcount
#   print(count, "Record inserted successfully into mobile table")
  

 
