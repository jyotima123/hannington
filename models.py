def saleList():
  saleListModel = {
        "SaleID": "VARCHAR(255) UNIQUE NOT NULL",
        "OrderNumber": "VARCHAR(100) NOT NULL",
        "Status": "VARCHAR(20) NOT NULL",
        "OrderDate": "TIMESTAMP NOT NULL",
        "InvoiceDate": "TIMESTAMP NULL",
        "Customer": "VARCHAR(255) NOT NULL",
        "CustomerID": "VARCHAR(255) NOT NULL",
        "InvoiceNumber": "VARCHAR(255) NULL",
        "CustomerReference": "VARCHAR(255) NULL",
        "InvoiceAmount": "float8",
        "PaidAmount": "float8",
        "InvoiceDueDate": "TIMESTAMP NULL",
        "ShipBy": "VARCHAR(255) NULL",
        "BaseCurrency": "VARCHAR(20) NULL",
        "CustomerCurrency": "VARCHAR(20) NULL",
        "CreditNoteNumber": "VARCHAR(255) NULL",
        "Updated": "TIMESTAMP NULL",
        "QuoteStatus": "VARCHAR(255) NULL",
        "OrderStatus": "VARCHAR(255) NULL",
        "CombinedPickingStatus": "VARCHAR(255) NULL",
        "CombinedPaymentStatus": "VARCHAR(255) NULL",
        "CombinedTrackingNumbers": "VARCHAR(255) NULL",
        "CombinedPackingStatus": "VARCHAR(255) NULL",
        "CombinedShippingStatus": "VARCHAR(255) NULL",
        "CombinedInvoiceStatus": "VARCHAR(255) NULL",
        "CreditNoteStatus": "VARCHAR(255) NULL",
        "FulFilmentStatus": "VARCHAR(255) NULL",
        "Type": "VARCHAR(255) NULL",
        "SourceChannel": "VARCHAR(255) NULL",
        "ExternalID": "VARCHAR(255) NULL",
        "OrderLocationID": "VARCHAR(255) NOT NULL",
      }
    
  saleList = "create table if not exists l0.saleList("

  for k in saleListModel.keys() :
    saleList += '"{}" {}, '.format(k,saleListModel[k])
    
  saleList = saleList[:-2]+");"
  return saleList

def Product():
  productModel = {
    "ID": "VARCHAR(255) UNIQUE NOT NULL",
    "SKU": "VARCHAR(255) UNIQUE NOT NULL",
    "Name": "VARCHAR(255) NULL",
    "Category": "VARCHAR(255) NULL",
    "Brand": "VARCHAR(255) NULL",
    "Type": "VARCHAR(50) NOT NULL",
    "CostingMethod": "VARCHAR(255) NULL",
    "DropShipMode": "VARCHAR(255) NULL",
    "DefaultLocation": "VARCHAR(255) NULL",
    "Length": "float8",
    "Width": "float8",
    "Height": "float8",
    "Weight": "float8",
    "UOM": "VARCHAR(255) NULL",
    "WeightUnits": "VARCHAR(255) NULL",
    "DimensionsUnits": "VARCHAR(255) NULL",
    "Barcode": "VARCHAR(255) NULL",
    "MinimumBeforeReorder": "float8",
    "ReorderQuantity": "float8",
    "PriceTier1": "float8",
    "PriceTier2": "float8",
    "PriceTier3": "float8",
    "PriceTier4": "float8",
    "PriceTier5": "float8",
    "PriceTier6": "float8",
    "PriceTier7": "float8",
    "PriceTier8": "float8",
    "PriceTier9": "float8",
    "PriceTier10": "float8",
    "AverageCost": "float8",
    "ShortDescription": "VARCHAR(550) NULL",
    "Description": "text NULL",
    "InternalNote": "text NULL",
    "DiscountRule": "VARCHAR(255) NULL",
    "Tags": "VARCHAR(255) NULL",
    "Status": "VARCHAR(50) NULL",
    "StockLocator": "VARCHAR(255) NULL",
    "COGSAccount": "VARCHAR(255) NULL",
    "RevenueAccount": "VARCHAR(255) NULL",
    "ExpenseAccount": "VARCHAR(255) NULL",
    "InventoryAccount": "VARCHAR(255) NULL",
    "PurchaseTaxRule": "VARCHAR(255) NULL",
    "SaleTaxRule": "VARCHAR(255) NULL",
    "LastModifiedOn": "TIMESTAMP NULL",
    "Sellable": "boolean",
    "PickZones": "VARCHAR(255) NULL",
    "BillOfMaterial": "boolean",
    "AutoAssembly": "boolean",
    "AutoDisassembly": "boolean",
    "QuantityToProduce": "float8",
    "AssemblyInstructionURL": "VARCHAR(255) NULL",
    "AssemblyCostEstimationMethod": "VARCHAR(255) NULL",
    "BOMType": "VARCHAR(100) NULL"
  }

  productMovement = {
    "ProductID":"VARCHAR(255) NOT NULL",
    "TaskID": "VARCHAR(255) NULL",
    "Type": "VARCHAR(255) NULL",
    "Date": "TIMESTAMP NULL",
    "Number": "VARCHAR(255) NULL",
    "Status": "int",
    "Quantity": "float8",
    "Amount": "float8",
    "Location": "VARCHAR(255) NULL",
    "BatchSN": "VARCHAR(255) NULL",
    "ExpiryDate": "TIMESTAMP NULL",
    "FromTo": "VARCHAR(255) NULL"
  }

  productCustomPrice = {
    "CustomerID": "VARCHAR(255) NULL",
    "CustomerName" : "VARCHAR(256) null",
    "ProductID": "VARCHAR(255) NULL",
    "ProductName": "VARCHAR(255) NULL",
    "ProductSKU": "VARCHAR(255) NULL",
    "Price": "float8"
  }

  productList = "create table if not exists dear_products("

  for k in productModel.keys() :
    productList += '"{}" {}, '.format(k,productModel[k])
    
  productList = productList[:-2]+"); create table if not exists dearProduct_movement("

  for k in productMovement.keys():
    productList += '"{}" {}, '.format(k,productMovement[k])

  productList = productList[:-2]+");create table if not exists dearProduct_customprice("

  for k in productCustomPrice.keys():
    productList += '"{}" {}, '.format(k,productCustomPrice[k])

  productList = productList[:-2]+");"    
  return productList

def purchaseList():
  purchaseModel = {
      "ID": "VARCHAR(255) UNIQUE NOT NULL",
      "BlindReceipt": "boolean",
      "OrderNumber": "VARCHAR(255) NULL",
      "Status": "VARCHAR(60) NULL",
      "OrderDate": "TIMESTAMP null",
      "InvoiceDate": "TIMESTAMP null",
      "Supplier": "VARCHAR(257) NULL",
      "SupplierID": "VARCHAR(257) NULL",
      "InvoiceNumber": "VARCHAR(257) NULL",
      "InvoiceAmount": "float8",
      "PaidAmount": "float8",
      "InvoiceDueDate": "TIMESTAMP null",
      "RequiredBy": "TIMESTAMP null",
      "BaseCurrency": "varchar(5) null",
      "SupplierCurrency": "varchar(5) null",
      "CreditNoteNumber": "varchar(255) null",
      "OrderStatus": "varchar(20) null",
      "StockReceivedStatus": "varchar(20) null",
      "UnstockStatus": "varchar(20) null",
      "InvoiceStatus": "varchar(20) null",
      "CreditNoteStatus": "varchar(20) null",
      "LastUpdatedDate": "TIMESTAMP null",
      "Type": "varchar(20) null",
      "CombinedInvoiceStatus": "varchar(30) null",
      "CombinedPaymentStatus": "varchar(30) null",
      "CombinedReceivingStatus": "varchar(30) null",
      "IsServiceOnly": "boolean",
      "DropShipTaskID": "varchar(260) null"
  } 

  purchaseList = "create table if not exists l0.dear_purchaselist("

  for k in purchaseModel.keys() :
    purchaseList += '"{}" {}, '.format(k,purchaseModel[k])
    
  purchaseList = purchaseList[:-2]+");"
  return purchaseList
