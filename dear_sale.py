import datetime as DT
import json
import psycopg2
import requests
import time
import models

class Dearinventory :
    
    def __init__(self):
        today = DT.date.today()
        self.createdSince = today - DT.timedelta(days=7)#$date->format('Y-m-d\TH:i:s\Z');
        # self.saveFile = "/var/www/html/dear_inventory/data_files/dear-"+str(today)+'.json';
        # query = 'Select * from apis limit 1';
        # result = self.getMysqlConn()->query($query)->fetch_assoc();
        # if(empty($result)){
        #     self.logs->logError( date('Y-m-d H:i:s')."- Keys not found in apis table" );
        #     die;
        # }
        self.baseUrl = "https://inventory.dearsystems.com/ExternalApi/v2/"
        
        self.allProducts = []
        self.allSales = []
        self.limit = 1000
        self.page = 1
        apiAuthAccountid =  "13a35284-84b0-497c-918d-b3589a2accb8"
        apiAuthApplicationkey = "0a7a3e04-3d1c-8d14-695d-6cbcab714bfe"
        self.headers = {
            "api-auth-accountid":apiAuthAccountid,
            "api-auth-applicationkey":apiAuthApplicationkey
        }
        self.getPgSqlConn()

        
    def getPgSqlConn(self) :
        host = "localhost"
        db_name = "smithey_db"
        username = "postgres"
        password = "Uch3W@#$wup1?B53:tg`Sot"
        port = 5432
        try :
            conn = psycopg2.connect(
                database=db_name, 
                user = username, 
                password = password, 
                host = host, 
                port = port
            )
            self.conn = conn
        except Exception as e:
            print("Unable to connect to database--"+str(e))
            exit()

    def callApi(self, table) :
        url = self.baseUrl+table+"?page={}&limit={}".format(self.page, self.limit)
        try : 
            salesListApi = requests.request("GET", url, headers=self.headers)
            if(salesListApi.text == 'Incorrect credentials!') :
                print('Incorrect credentials!')
                exit()
            else :
                resp = salesListApi.json()
                if(resp['Total'] > 0) :
                    # for salesList
                    # self.salesData(table, resp)

                    #for products
                    # self.productData(table, resp['Products'])

                    #for purchase list
                    self.purchaseData(table,resp)
                    if( resp['Total']  > (self.limit * self.page) ) :
                        self.page = self.page + 1
                        self.allSales = []
                        self.callApi(table)
                    print('completed')   
                else :
                    print('No records found')

        except Exception as e :
            print(str(e))
            exit()


    def salesData(self, table, allData) :
       
        #Data processing starts
        cur = self.conn.cursor() 
        createTable = getattr(models, '%s' % table)()
        cur.execute(createTable)
        self.conn.commit()
        #for sales in allData['SaleList'] :
        data = json.dumps(allData['SaleList'])
        query_sql = """ insert into l0."""+table+""" select * from json_populate_recordset(NULL::l0."""+table+""", %s) """
        print(query_sql)
        cur.execute(query_sql, (data,))
        self.conn.commit()
        time.sleep(2)
        exit()

        #Data processing ends

        #json file creation starts
        
        #self.createFile(self.allSales)

        #$bigQuery = new BigQueryAuth;
        #$bigQuery->uploadFile($this->saveFile);

        # // echo "<pre>";print_r($this->allSales);
        # // print_r(json_encode($this->allSales));
        # // die;
        # // // $sales = new Sales();
        # // $products = new Products();
        # // $sales->saveQuery($this->allSales, $this->getMysqlConn());
        # // $products->saveQuery($this->allProducts, $this->getMysqlConn());
        
    def productData(self, table, allData) :
       
        #Data processing starts
        cur = self.conn.cursor() 
        createTable = getattr(models, '%s' % table)()
        cur.execute(createTable)
        self.conn.commit()

        length = len(allData)
        products = []
        movement = []
        customP = []
        
        for i in range(length):
          products.append(allData[i])
          movementLength = len(allData[i]['Movements'])
          if(movementLength > 0):
            for j in range(movementLength):
                movedata = {
                "ProductID": allData[i]['ID'],
                "TaskID": allData[i]['Movements'][j]['TaskID'],
                "Type": allData[i]['Movements'][j]['Type'],
                "Date": allData[i]['Movements'][j]['Date'],
                "Number": allData[i]['Movements'][j]['Number'],
                "Status": allData[i]['Movements'][j]['Status'],
                "Quantity": allData[i]['Movements'][j]['Quantity'],
                "Amount": allData[i]['Movements'][j]['Amount'],
                "Location": allData[i]['Movements'][j]['Location'],
                "BatchSN": allData[i]['Movements'][j]['BatchSN'],
                "ExpiryDate": allData[i]['Movements'][j]['ExpiryDate'],
                "FromTo": allData[i]['Movements'][j]['FromTo']
                }
                movement.append(movedata)
          customLength = len(allData[i]['CustomPrices'])
          if(customLength > 0):
            for k in range(customLength):
                customdata = {
                    "CustomerID": allData[i]['CustomPrices'][k]['CustomerID'],
                    "CustomerName" : allData[i]['CustomPrices'][k]['CustomerName'],
                    "ProductID": allData[i]['CustomPrices'][k]['ProductID'],
                    "ProductName": allData[i]['CustomPrices'][k]['ProductName'],
                    "ProductSKU": allData[i]['CustomPrices'][k]['ProductSKU'],
                    "Price": allData[i]['CustomPrices'][k]['Price']
                }    
                customP.append(customdata)  

        # insert data into product table
        query_sql = """ insert into l0.dear_products select * from json_populate_recordset(NULL::l0.dear_products, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(products),))
        self.conn.commit()
        time.sleep(2)

        #insert data into movement table
        query_sql = """ insert into l0.dearProduct_movement select * from json_populate_recordset(NULL::l0.dearProduct_movement, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(movement),))
        self.conn.commit()
        time.sleep(2)

        #insert data into custom price table
        query_sql = """ insert into l0.dearProduct_customprice select * from json_populate_recordset(NULL::l0.dearProduct_customprice, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(customP),))
        self.conn.commit()
        time.sleep(2)

           
    def purchaseData(self, table, allData) :
       
        #Data processing starts
        cur = self.conn.cursor() 
        createTable = getattr(models, '%s' % table)()
        cur.execute(createTable)
        self.conn.commit()
        #for sales in allData['SaleList'] :
        data = json.dumps(allData['PurchaseList'])
        query_sql = """ insert into l0.dear_purchaselist select * from json_populate_recordset(NULL::l0.dear_purchaselist, %s) """
        print(query_sql)
        cur.execute(query_sql, (data,))
        self.conn.commit()
        time.sleep(2)
        


         
      
Dear = Dearinventory()
tables = ['purchaseList']
for table in tables:
    Dear.callApi(table)