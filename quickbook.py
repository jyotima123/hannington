import json
import psycopg2
import requests
import time
import quick_models

class profitLoss:
    def __init__(self):
        
        self.baseUrl = "https://quickbooks.api.intuit.com/v3/company/4620816365234118390/"
        self.start_date = '2022-06-01'
        self.end_date = '2022-06-15'
        self.summarize = 'Days'
        self.refreshToken = 'AB11667726169MaJ5rpsmAStl5Y6H9lBlco9vJZ7EvfjZUh1TO'
        self.headers = {
            "Accept-type":"application/json",
            "content-type":"application/json",
            "authorization":"Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..0wJ8uAcyh7W_YUVjIwYsvw.T3sDYcScqpUnlTAWXIhGpbS8GoUQiFochbuekY1KZ_s6cRJYFXkoBXYJ9S8I_K7UXJiQX2OJ5qQDdq2q18GNaAgrZYkPXH3a4Mu7op8zW3aCZtFiD9Nydci7POiyvWYH4Zn14BgZGstwZUHfypQJfkHbtpIRX_M8KcdNtcQuNtZoWbrvNEdMp3oLrmApC5N6bevE503aCopBb3V19TS1qnXoO4R5phHky26iIEHpQu3erWbl1j2uz9aa2zk_cWX5ar-Cf0lMWqjRnLD0DzMNryMkMGtok_q3RaHBx90LP3zw7T5wBhIXamp2GlHVdWGjoc3eGDrlkf1XNkGrKlrwfGIaoqDKV6mHDb2u2Mkfl42DgOiiLWVp1NFCo-bh5edHm7zmZCJoIjGta5Ega2i-J4xOYSwCYbaLZQKp-doT0gNoMSMB8l4UoLwOatkCdVyFw7TWoguR_P8xaC5t1w1_-O1ltfqa4ZcDrpqQ_ojU5C5sbs18iWuxlW9wYjWbuEd1WB-vAyEDPQkyZ96hms5R0Qa64x6UVencGlsfR2GdVN5ZuV-I8hu-pO649oEVarjysLgF7P7N5PK3_bhmwoBzKZx3eYH6YY6zGPm-vkJ8pdDOiwRkJ24d53BpQI0Bo3khQs6SXXGVTO2O-hGHj2v2G_bhVB8WvT8SZtnr2-0S74XTcT6BhJvbZk0lJb2wvVb5d2DYcAxQ_B7-ReXT6LfWM7Yx_ahOaTl0auAOm1NMI4D1CcjknMBJTKtgb82hpMS-.ycKAdUGr-6ZaSCKsz4mJfA"
        }
        self.getPgSqlConn()

    # def generateToken(self):
    #     url = 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer'


    def getPgSqlConn(self) :
        host = "localhost"
        db_name = "bi"
        username = "postgres"
        password = "Jyotima@123"
        port = 5432
        try :
            conn = psycopg2.connect(
                database=db_name, 
                user = username, 
                password = password, 
                host = host, 
                port = port
            )
            self.conn = conn
        except Exception as e:
            print("Unable to connect to database--"+str(e))
            exit()

    def callApi(self, table) :
        url = self.baseUrl+"reports/ProfitAndLoss?start_date={}&end_date={}&summarize_column_by={}".format(self.start_date,self.end_date,self.summarize)
        try : 
            profitLossApi = requests.request("GET", url, headers=self.headers)
            print(profitLossApi)
            exit()
            # if(profitLossApi.status_code == 401) :
            #     self.generateToken()
            # else :
            #     resp = profitLossApi.json()
            #     self.orderData(table, resp)
            #     print('completed')   
                

        except Exception as e :
            print(str(e))
            exit() 

    def orderData(self, table, allData) :

       #create table
       cur = self.conn.cursor() 
       createTable = getattr(quick_models, '%s' % table)()
       cur.execute(createTable)
       self.conn.commit()

       #Data processing starts
       groupArray = ['Income','Expenses','OtherExpenses']
       length = len(allData['Rows']['Row'])
       data = allData['Rows']['Row']
       profitLoss = []
       for i in range(length):
         if(data[i]['group'] in groupArray) :  
            len1 = len(data[i]['Rows']['Row'])
            for j in range(len1):
                if(data[i]['Rows']['Row'][j]['type'] == 'Section'):
                    self.rowDetail(data[i]['Rows']['Row'][j],allData['Columns']['Column'],profitLoss)
                else:
                   self.colDetail(data[i]['Rows']['Row'][j]['ColData'],allData['Columns']['Column'],profitLoss)
                    
       query_sql = """ insert into """+table+""" select * from json_populate_recordset(NULL::"""+table+""", %s) """
       print(query_sql)
       cur.execute(query_sql, (json.dumps(profitLoss),))
       self.conn.commit()
       time.sleep(2)

       exit()

    def rowDetail(self,rows,colval,profitLoss):
        len2 = len(rows['Rows']['Row'])
        for i in range(len2):
            if(rows['Rows']['Row'][i]['type']=='Section'):
                self.rowDetail(rows['Rows']['Row'][i],colval,profitLoss)
            else:
                self.colDetail(rows['Rows']['Row'][i]['ColData'],colval,profitLoss)
                

    def colDetail(self,cols,colval,profitLoss):
        len2 = len(cols)
        for i in range(len2-1):
            if i == 0 :
                date = ''
                account = cols[i]['value']
                money = 0.00
            else :
                date = colval[i]['ColTitle']
                money = 0.00 if cols[i]['value'] == '' else cols[i]['value']
            if i > 0:    
                report = {
                    "account": account,
                    "date": date,
                    "money": money,
                } 
                profitLoss.append(report)
           

         
profit = profitLoss()
tables = ['profitloss_report']
for table in tables:
    profit.callApi(table)


# Production Base URL:https://quickbooks.api.intuit.com
