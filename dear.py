from urllib.request import urlopen,Request
import psycopg2
import json

conn = psycopg2.connect(host="localhost",database="bi",user="postgres",password="Jyotima@123",port=5432)
# create connection object testing again run this agaian tes   t  in  g again
cursor = conn.cursor()

headers = {
  'Content-Type': 'application/json',
  'api-auth-accountid': '13a35284-84b0-497c-918d-b3589a2accb8',
  'api-auth-applicationkey': '0a7a3e04-3d1c-8d14-695d-6cbcab714bfe'
}
request = Request('https://inventory.dearsystems.com/ExternalApi/v2/saleList?Page=1&Limit=5', headers=headers)

response_body = urlopen(request).read()
resultData = json.loads(response_body)
length = len(resultData['SaleList'])
for i in range(length):
  postgres_insert_query = """ INSERT INTO l0.dear_inventory (category,data) VALUES (%s,%s)"""
  record_to_insert = ('salelist', json.dumps(resultData['SaleList'][i]))
  cursor.execute(postgres_insert_query, record_to_insert)
  conn.commit()
  count = cursor.rowcount
  print(count, "Record inserted successfully into mobile table")
