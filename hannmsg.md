Hi Hannington, I've found a solution to the Shopify issue. 

I will share two URLs with you and the same will need to be opened one by one on the same browser (either in the same tab or in a different one). 

So, whoever is doing it will need to click on the first URL first, It will return an URL ,copy and save it somewhere and then hit the second URL for the app installation button to get active (and turn green). Whenever there is a gap in time between opening the two or a change of browsers, Shopify fails to recognise it and the app installation button does not become active.

1st one is : 

https://smithey-iron-ware.myshopify.com/admin/oauth/install_custom_app?client_id=b2b74fc101ae2a312bcea58086167cea&signature=eyJfcmFpbHMiOnsibWVzc2FnZSI6ImV5SmxlSEJwY21WelgyRjBJam94TmpVNE9USTNOalUyTENKd1pYSnRZVzVsYm5SZlpHOXRZV2x1SWpvaWMyMXBkR2hsZVMxcGNtOXVMWGRoY21VdWJYbHphRzl3YVdaNUxtTnZiU0lzSW1Oc2FXVnVkRjlwWkNJNkltSXlZamMwWm1NeE1ERmhaVEpoTXpFeVltTmxZVFU0TURnMk1UWTNZMlZoSWl3aWNIVnljRzl6WlNJNkltTjFjM1J2YlY5aGNIQWlmUT09IiwiZXhwIjoiMjAyMi0wOC0wM1QxMzoxNDoxNi4wOTFaIiwicHVyIjpudWxsfX0%3D--dc99b5ab570d25c25678b83ac0743cdd4e1f8b0d

2nd one is: 

https://smithey-iron-ware.myshopify.com/admin/oauth/authorize?client_id=b2b74fc101ae2a312bcea58086167cea&scope=write_orders,read_customers&redirect_uri=http://wksystems.net/page/home