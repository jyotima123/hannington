def orders():
    orderListModel = {
        "id": "VARCHAR(255) UNIQUE NOT NULL",
        "created_at": "TIMESTAMP NOT NULL",
        "currency": "VARCHAR(20) NULL",
        "current_subtotal_price": "float8",
        "current_total_discounts": "float8",
        "current_total_price": "float8",
        "current_total_tax": "float8",
        "email": "VARCHAR(50) NULL",
        "contact_email": "VARCHAR(50) NULL",
        "financial_status": "VARCHAR(20) NULL",
        "name": "VARCHAR(120) NULL",
        "number": "VARCHAR(255) NULL",
        "order_number": "VARCHAR(255) NULL",
        "subtotal_price": "float8",
        "total_discounts": "float8",
        "total_line_items_price": "float8",
        "total_outstanding": "float8",
        "total_price": "float8",
        "total_price_usd": "float8",
        "total_shipping_price_set": "json",
        "total_tax": "float8",
        "total_tip_received": "float8",
        "total_weight": "float8",
        "updated_at": "TIMESTAMP NOT NULL",
        
        }
    taxLine = {
            "order_no": "varchar(255) not null",
            "price": "float8",
            "rate": "float8",
            "title": "varchar(100) not null",
    }
    lineItems = {
        "order_no" : "varchar(255) not null",
        "id": "varchar(150) not null",
        "fulfillable_quantity": "int",
        "grams": "varchar(150) null",
        "name": "varchar(150) null",
        "pre_tax_price": "float8",
        "price": "float8",
        "product_id": "varchar(150) null",
        "quantity": "int",
        "requires_shipping": "boolean",
        "sku": "varchar(150) null",
        "tax_code": "varchar(150) null",
        "taxable": "boolean",
        "title": "varchar(150) null",
        "total_discount": "float8",
        "vendor": "varchar(150) null",
    }  
    itemtaxLine = {
            "price": "float8",
            "rate": "float8",
            "title": "varchar(100) not null",
            "lineItem": "varchar(255) not null"
    }
    shippingLines = {
        "order_no" : "varchar(255) not null",
        "id": "varchar(100) null",
        "code": "varchar(100) null",
        "discounted_price": "float8",
        "price": "float8",
        "title": "varchar(100) null",
    }
    shippingtaxLines = {
        "price": "float8",
        "rate": "float8",
        "title": "varchar(100) not null",
        "shipId": "varchar(255) not null"
    }

    orderList = "create table if not exists orderList("

    for k in orderListModel.keys() :
        orderList += '"{}" {}, '.format(k,orderListModel[k])
    
    orderList = orderList[:-2]+"); create table if not exists taxLine("

    for k in taxLine.keys():
        orderList += '"{}" {}, '.format(k,taxLine[k])

    orderList = orderList[:-2]+");create table if not exists lineItems("

    for k in lineItems.keys():
        orderList += '"{}" {}, '.format(k,lineItems[k])

    orderList = orderList[:-2]+");create table if not exists itemtaxLine("

    for k in itemtaxLine.keys():
        orderList += '"{}" {}, '.format(k,itemtaxLine[k])

    orderList = orderList[:-2]+");create table if not exists shippingLines("

    for k in shippingLines.keys():
        orderList += '"{}" {}, '.format(k,shippingLines[k])

    orderList = orderList[:-2]+");create table if not exists shippingtaxLines("

    for k in shippingtaxLines.keys():
        orderList += '"{}" {}, '.format(k,shippingtaxLines[k])

    orderList = orderList[:-2]+");"                
    return orderList