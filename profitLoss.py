import datetime as DT
import json
import psycopg2
import requests
import time
import quick_models

class ProfitLossReport :
    
    def __init__(self):
       
        self.baseUrl = "https://sandbox-quickbooks.api.intuit.com/v3/"
        
        # self.allProducts = []
        # self.allSales = []
        # self.limit = 1000
        # self.page = 1
        # apiAuthAccountid =  "13a35284-84b0-497c-918d-b3589a2accb8"
        # apiAuthApplicationkey = "0a7a3e04-3d1c-8d14-695d-6cbcab714bfe"
        self.headers = {
            "Accept-type":"application/json",
            "content-type":"application/json",
            "authorization":"Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..ltcJfwRde_JdqPIlCM_ZTg.AJ7OAWfo1DKrA-69BRO7tGHtwfhwFKpQcZ4heTqI84oePO69WjZQyIUsqUzJ6qdd-knePcloEy2HENSnR0BEjvUJ2jyQozNuN2OLOrTqWRN_vBmj9yhcplqVXGcwWpEbZGLL-HeIRccwxJdzca1sictVpznEOoKY65rtIZD8TVds-GL8_6CoRojrhmVCxVjmSNQTOKEc3kAe5yi1f4a7g8jOsyWUPluW4TAXb7OZV50vKbnbr90PLu2g1cgNoQWD1tApCIDHGN5IwPKPn_Hfj_Ra_MeFJl3JwBsAtYXBrfUz-PYSkXbl4QEmFnRw1l9zdbDLeAVlk2TipzQRSD9-osc8-dWMBNZgjZ0Gjc6zeAzBf651bpn7Tf9T8n86PHWxKEuH15ZiFIv0DPZZrGdP0KGE19KcRpXyo1trMlTnLpKTtcRmA3ODIicYR01nVEpK5t2jyb45-__W_tdr1B9fZVT06gBpz3tPmAsTHUssxk3kbHoyrhi4Ky20ukMNKpQ84etSvDSYqOqLr-Gvs_0z38Y7gJgGrx-IsM0dEHw0vd9MSbOObOaoFnWIHeCEqtiJXWJ2HpMGJ2ajxCw5EyWbk1OWwoczCvqSzP6sZRTFM-91q8SSoHti2fCUlJ7KTpj04OggOCq9G4OeilUlYt40KYYfK7rLWxAeD3Jx1sUpmAsdr8saTQ7AtXAEYAWpBSvQk17e8q2rHr4k8riRsAKjJ_EEHmoLj7Ta4uXj-hNWQVs4Zfqp_L2g5SdSTBazi8Pk.qT-GbmfDxFkWTjLjwmYwng"
        }
        self.getPgSqlConn()

        
    def getPgSqlConn(self) :
        host = "localhost"
        db_name = "bi"
        username = "postgres"
        password = "Jyotima@123"
        port = 5432
        try :
            conn = psycopg2.connect(
                database=db_name, 
                user = username, 
                password = password, 
                host = host, 
                port = port
            )
            self.conn = conn
        except Exception as e:
            print("Unable to connect to database--"+str(e))
            exit()

    def callApi(self, table) :
        url = self.baseUrl+"/company/4620816365234118390/reports/ProfitAndLoss?start_date=2022-06-01&end_date=2022-06-15&summarize_column_by=Days"
        try : 
            salesListApi = requests.request("GET", url, headers=self.headers)
            if(salesListApi.text == 'Incorrect credentials!') :
                print('Incorrect credentials!')
                exit()
            else :
                resp = salesListApi.json()
                print(resp)

        except Exception as e :
            print(str(e))
            exit()
   
Dear = ProfitLossReport()
tables = ['profitLossList']
for table in tables:
    Dear.callApi(table)