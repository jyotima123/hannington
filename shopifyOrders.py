import shopify
import json
import psycopg2
import requests
import time
import shopify_model
# api_key="b2b74fc101ae2a312bcea58086167cea"
# secret="8cbe5d3e58e75c973a8745b6ff3277cd"
#db password = Uch3W@#$wup1?B53:tg`Sot

# shop_url = "smithey-iron-ware.myshopify.com"
# api_version = '2022-07'
# access_token = "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"

# session = shopify.Session(shop_url, api_version, access_token)
# shopify.ShopifyResource.activate_session(session)
# # get the current shop
# shop = shopify.Shop.current()

# # get orders listorderListApi = requests.request("GET", url, headers=headers)
# resp = orderListApi.json()

# order = shopify.Order.find(4585208709277)
# print(order)url = orders.json?status=any"
class shopifyOrder:
    def __init__(self):
        
        self.baseUrl = "https://smithey-iron-ware.myshopify.com/admin/api/2022-07/"
        self.status = 'any'
        self.fromDate = '2022-06-11T00:00:00-04:00'
        self.toDate = '2022-06-11T05:00:00-04:00'
        self.limit = 3
        self.id= 4528132915357
        self.headers = {
            "X-Shopify-Access-Token":"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"
        }
        self.getPgSqlConn()

    def getPgSqlConn(self) :
        host = "localhost"
        db_name = "bi"
        username = "postgres"
        password = "Jyotima@123"
        port = 5432
        try :
            conn = psycopg2.connect(
                database=db_name, 
                user = username, 
                password = password, 
                host = host, 
                port = port
            )
            self.conn = conn
        except Exception as e:
            print("Unable to connect to database--"+str(e))
            exit()

    def callApi(self, table) :
        url = self.baseUrl+table+".json?status={}&since_id={}&limit={}".format(self.status,self.id,self.limit)
        # url = self.baseUrl+"orders/count.json?since_id={}&created_at_max={}&created_at_min={}&status={}&since_id={}&limit={}".format(self.id,self.toDate,self.fromDate,self.status,self.limit)
        try : 
            orderListApi = requests.request("GET", url, headers=self.headers)
            if(orderListApi.text == 'Incorrect credentials!') :
                print('Incorrect credentials!')
                exit()
            else :
                resp = orderListApi.json()
                orderId = self.orderData(table, resp['orders'])
                if( resp['orders'] ) :
                        self.id = orderId
                        self.callApi()
                print('completed')   
                

        except Exception as e :
            print(str(e))
            exit() 

    def orderData(self, table, allData) :
       
        #Data processing starts
        cur = self.conn.cursor() 
        # createTable = getattr(shopify_model, '%s' % table)()
        # cur.execute(createTable)
        # self.conn.commit()
        #for orders in allData['orders'] :
        
        length = len(allData)
        orders = []
        taxlines = []
        lineItems = []
        itemTax = []
        shipping = []
        shippingTax = []
        
        for i in range(length):
          orders.append(allData[i])
          taxlineLength = len(allData[i]['tax_lines'])
          if(taxlineLength > 0):
            for j in range(taxlineLength):
                itemdata = {
                "order_no" : allData[i]['id'],
                "price" : allData[i]['tax_lines'][j]['price'],
                "rate": allData[i]['tax_lines'][j]['rate'],
                "title" : allData[i]['tax_lines'][j]['title']
                }
                taxlines.append(itemdata) 
          lineLength = len(allData[i]['line_items'])
          if(lineLength > 0):
            for k in range(lineLength):
                linedata = {
                    "order_no" : allData[i]['id'],
                    "id": allData[i]['line_items'][k]['id'],
                    "fulfillable_quantity": allData[i]['line_items'][k]['fulfillable_quantity'],
                    "grams": allData[i]['line_items'][k]['grams'],
                    "name": allData[i]['line_items'][k]['name'],
                    "pre_tax_price": allData[i]['line_items'][k]['pre_tax_price'],
                    "price": allData[i]['line_items'][k]['price'],
                    "product_id": allData[i]['line_items'][k]['product_id'],
                    "quantity": allData[i]['line_items'][k]['quantity'],
                    "requires_shipping": allData[i]['line_items'][k]['requires_shipping'],
                    "sku": allData[i]['line_items'][k]['sku'],
                    "tax_code": allData[i]['line_items'][k]['tax_code'],
                    "taxable": allData[i]['line_items'][k]['taxable'],
                    "title": allData[i]['line_items'][k]['title'],
                    "total_discount": allData[i]['line_items'][k]['total_discount'],
                    "vendor": allData[i]['line_items'][k]['vendor'],
                }
                lineItems.append(linedata)
                itemTaxlength = len(allData[i]['line_items'][k]['tax_lines'])
                if(itemTaxlength > 0):
                    for ki in range(itemTaxlength):
                        itemtaxdata = {
                        "price" : allData[i]['line_items'][k]['tax_lines'][ki]['price'],
                        "rate": allData[i]['line_items'][k]['tax_lines'][ki]['rate'],
                        "title" : allData[i]['line_items'][k]['tax_lines'][ki]['title'],
                        "lineItem": allData[i]['line_items'][k]['id'],
                        }
                    itemTax.append(itemtaxdata)
          shiplineLength = len(allData[i]['shipping_lines'])
          if(shiplineLength > 0):
            for l in range(shiplineLength):
                shipdata = {
                    "order_no" : allData[i]['id'],
                    "id": allData[i]['shipping_lines'][l]['id'],
                    "code": allData[i]['shipping_lines'][l]['code'],
                    "discounted_price": allData[i]['shipping_lines'][l]['discounted_price'],
                    "price": allData[i]['shipping_lines'][l]['price'],
                    "title": allData[i]['shipping_lines'][l]['title'],
                }
                shipping.append(shipdata)
                shipTaxlength = len(allData[i]['shipping_lines'][l]['tax_lines'])
                if(shipTaxlength > 0):
                    for kl in range(shipTaxlength):
                        shiptaxdata = {
                        "price" : allData[i]['shipping_lines'][l]['tax_lines'][kl]['price'],
                        "rate": allData[i]['shipping_lines'][l]['tax_lines'][kl]['rate'],
                        "title" : allData[i]['shipping_lines'][l]['tax_lines'][kl]['title'],
                        "shipId": allData[i]['shipping_lines'][l]['id'],
                        }
                    shippingTax.append(shiptaxdata) 

        
        # # insert record into orders table
        query_sql = """ insert into orderList select * from json_populate_recordset(NULL::orderList, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(orders),))
        self.conn.commit()
        time.sleep(2)
        # insert data into taxlines
        query_sql = """ insert into taxLine select * from json_populate_recordset(NULL::taxLine, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(taxlines),))
        self.conn.commit()
        time.sleep(2)
        # insert data into lineItems
        query_sql = """ insert into lineItems select * from json_populate_recordset(NULL::lineItems, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(lineItems),))
        self.conn.commit()
        time.sleep(2)
        # insert data into linetaxItems
        query_sql = """ insert into itemtaxLine select * from json_populate_recordset(NULL::itemtaxLine, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(itemTax),))
        self.conn.commit()
        time.sleep(2)
        # insert data into shipping
        query_sql = """ insert into shippingLines select * from json_populate_recordset(NULL::shippingLines, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(shipping),))
        self.conn.commit()
        time.sleep(2)
        # insert data into shippingtax
        query_sql = """ insert into shippingtaxLines select * from json_populate_recordset(NULL::shippingtaxLines, %s) """
        print(query_sql)
        cur.execute(query_sql, (json.dumps(shippingTax),))
        self.conn.commit()
        time.sleep(2)
        return allData[length-1]['id']
               

Dear = shopifyOrder()
tables = ['orders']
for table in tables:
    Dear.callApi(table)