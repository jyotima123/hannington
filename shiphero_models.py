def ship_orders():
  saleListModel = {
          "id" : "varchar(255) unique not null",
          "legacy_id" : "varchar(255) not null",
          "order_number": "varchar(255) not null",
          "partner_order_id": "varchar(255) not null",
          "shop_name" : "varchar(255) not null",
          "fulfillment_status" : "varchar(100) null",
          "order_date": "TIMESTAMP NOT NULL",
          "total_tax" : "float8",
          "subtotal" : "float8",
          "total_discounts" : "float8",
          "total_price" : "float8",
          "auto_print_return_label":"boolean",
          "custom_invoice_url": "varchar(255) null",
          "account_id": "varchar(255) null",
          "email":"varchar(100) null",
          "profile":"varchar(255) null",
          "packing_note":"varchar(255) null",
          "required_ship_date":"varchar(150) null",
          "flagged":"boolean",
          "saturday_delivery": "boolean",
          "ignore_address_validation_errors": "boolean",
          "priority_flag":"boolean",
          "source": "varchar(100) null",
          "allow_partial" : "boolean",
          "require_signature" : "boolean",
          "adult_signature_required" : "boolean",
          "alcohol" : "boolean",
          "expected_weight_in_oz": "float8",
          "insurance" : "boolean",
          "insurance_amount": "float8",
          "currency": "varchar(5) null",
          "has_dry_ice": "boolean",
          "allocation_priority": "int",
          "allow_split": "boolean"
        
        }
  listItem = {
  "id" : "varchar(155) not null",
  "sku" : "varchar(255) null",
  "quantity" : "float8"
}   
    
  saleList = "create table if not exists ship_orders("

  for k in saleListModel.keys() :
    saleList += '"{}" {}, '.format(k,saleListModel[k])
    
  saleList = saleList[:-2]+"); create table if not exists shipOrdersLineitems("

  for k in listItem.keys():
    saleList += '"{}" {}, '.format(k,listItem[k])

  saleList = saleList[:-2]+");"  
  return saleList
  